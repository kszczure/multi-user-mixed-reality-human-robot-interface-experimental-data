# Multi-user Mixed Reality Human-Robot Interface for Teleoperation in Hazardous Environments - experimental data

**The structure of the folders follow the experiments classification of the published paper.

In single-user there is 1 file per folder and for multi-user there are 2 files (a file per user) per folder.


In the recordings there are following columns (explained in brackets [] if not self-explanatory):**

Timestamp (ms)

EventTypeString [OK_POINTCLOUD -> event when a point cloud was successfuly received; KO_POINTCLOUD -> event when a point cloud was corrupted; INTERVAL_LOG -> periodic recording of all parameters]

Resolution	

Real FPS (Hz)

Requested FPS (Hz)

Camera FrameBytes Length [size of video frame]

Downlink + Uplink Throughput (Mbps)	

Subsampling Unit Size (mm)	

Last Point Cloud Points Number	

Downlink Throughput (Mbps)	

Uplink Throughput (Mbps)	

RTT (ms)	

Estimated RTT (ms)	

TCP Estimated Time Out Interval (ms)	

Sender Bandwidth (Mbps)	Receiver Bandwidth (Mbps)	

Throughput / Bandwidth Ratio (percent)	

Uplink Downlink Ratio	

Unity Game FPS (Hz)	[Interface FPS]

[automatic settings - related settings]

Automatic Settings Enable	

Video or Point Cloud [selector of automatic setting mode]

Setting to control [selector of automatic setting mode]

Round Trip Time Target (ms)	

Throughput / Bandwidth Ratio Target (percent)	

FPS minimum limit (Hz)	

FPS maximum limit (Hz)	

Subsampling minimum limit (m)	

Subsampling maximum limit (m)	

[End of automatic settings - related settings]

Resolution option (LOW/MED/HIGH - 1/2/3)	

Camera name	

Throughput camera point cloud (Mbps)	

Throughput camera video stream (Mbps)	

Framerate delayed [event when the real FPS of video or point cloud was delayed >2Hz than requested FPS]

Control mode used [general robot control mode]

Real video FPS (Hz)

Real point cloud FPS (Hz)
